# Earthdata Taxonomy Select

## Contents of this File

* Introduction
* Requirements
* Installation
* Usage
* Maintainers


### Introduction

The Earthdata Taxonomy Select module allows for the selection and import of taxonomy terms from an external source.


### Requirements

* N/A


### Installation

* Install as you would normally install a contributed Drupal module.
   See: [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

### Usage

Taxonomy terms from an external source can be browsed, selected, and added to Drupal's taxonomy.


### Maintainers/Support

* Scott Brenner: [scott.brenner@ssaihq.com](mailto:scott.brenner@ssaihq.com)
* SSAI: [https://www.ssaihq.com](https://www.ssaihq.com)