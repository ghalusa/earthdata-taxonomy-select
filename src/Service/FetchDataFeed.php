<?php

/**
 * @file
 * Contains \Drupal\earthdata_taxonomy_select\Service\FetchDataFeed.
 */

namespace Drupal\earthdata_taxonomy_select\Service;

class FetchDataFeed {

  /**
   * @var $gcmdKmsApiUrl
   */
  // private $gcmdKmsApiUrl;

  /**
   * Creates a FetchDataFeed instance.
   */
  // public function __construct()
  // {
  //   $this->gcmdKmsApiUrl = 'https://gcmd.earthdata.nasa.gov/kms/concept/';
  // }

  // public static function create(ContainerInterface $container) {
  //   return new static(
  //     $container->get('request_stack'),
  //     $container->get('renderer'),
  //     $container->get('messenger'),
  //   );
  // }

  /**
   * Fetches JSON data from a given feed.
   *
   * @param string $url URL of a JSON data feed.
   *
   * @return mixed @see json_decode(). Decoded JSON is returned as an
   *   associative array.
   * @throws \Exception
   */
  public function fetchJsonData($url) {
    $parsedUrl = static::parseUrl($url);

    // To avoid fetching cached response.
    $parsedUrl['query']['timestamp'] = time();

    try {
      $client = new \GuzzleHttp\Client([
        'base_uri' => $parsedUrl['scheme'] . '://' . $parsedUrl['host'],
        'verify' => FALSE, // ignore invalid ssl
      ]);

      $options = [
        'query' => $parsedUrl['query'],
      ];

      if (isset($parsedUrl['user']) && isset($parsedUrl['pass'])) {
        $options['auth'] = [
          $parsedUrl['user'],
          $parsedUrl['pass']
        ];
      }

      $response = $client->get($parsedUrl['path'], $options);
      if ($response->getStatusCode() != '200') {
        throw new \Exception('Invalid response received from the feed.');
      }

      return json_decode($response->getBody(), TRUE);
    }
    catch(\Exception $e) {
      throw new \Exception('Something went wrong while making connection with the feed.');
    }
  }

  /**
   * Parse a URL and return its components. Extends the builtin parse_url()
   * function. Converts querystring to an associative array.
   *
   * @param string $url The URL to parse.
   *
   * @return array An associative array containing URL components.
   * @see parse_url()
   */
  public function parseUrl($url) {
    $parsedUrl = parse_url($url);

    if (isset($parsedUrl['query'])) {
      $params = [];
      parse_str($parsedUrl['query'], $params);
      $parsedUrl['query'] = $params;
    }

    return $parsedUrl;
  }

}
